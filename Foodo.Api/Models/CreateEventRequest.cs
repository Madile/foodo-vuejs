using System;

namespace Foodo.Api.Models
{
    public class CreateEventRequest
    {
        //public int EventStatusId { get; set; }
        //public int EventCategoryId { get; set; }
        public string EventTitle { get; set; }
        public string CreatedUsername { get; set; }
        public string EventDescription { get; set; }
        public DateTime ValidUntil { get; set; }
    }
}
