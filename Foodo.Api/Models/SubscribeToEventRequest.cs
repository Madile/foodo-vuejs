﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foodo.Api.Models
{
    public class SubscribeToEventRequest
    {
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public int EventId { get; set; }
    }
}
