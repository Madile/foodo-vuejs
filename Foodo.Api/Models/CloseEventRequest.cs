﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foodo.Api.Models
{
    public class CloseEventRequest
    {
        public int EventId { get; set; }
    }
}
