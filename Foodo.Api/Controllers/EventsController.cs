﻿using Foodo.Api.Models;
using Foodo.Core.Common;
using Foodo.Core.Models;
using Foodo.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Foodo.Api.Controllers
{
    [Route("api/[controller]")]
    public class EventsController : BaseController
    {
        private readonly EventService _service;

        public EventsController(EventService service)
        {
            _service = service;
        }

        [HttpPost, Authorize]
        [Route("Create")]
        public ServiceResponse Create([FromBody]CreateEventRequest r)
        {
            return _service.Create(r.EventTitle, r.EventDescription, r.ValidUntil, 0, this.GetCurrentUserId());
        }

        [HttpGet, Authorize]
        [Route("GetList")]
        public List<EventInfo> GetList()
        {
            return this._service.GetList();
        }

        [HttpPost, Authorize]
        [Route("Subscribe")]
        public ServiceResponse Subscribe([FromBody]SubscribeToEventRequest r)
        {
            return this._service.Subscribe(r.EventId, r.Description, this.GetCurrentUserId(), r.Amount);
        }

        [HttpPost, Authorize]
        [Route("Close")]
        public ServiceResponse Close([FromBody]CloseEventRequest r)
        {
            return this._service.Close(r.EventId);
        }

        [HttpGet, Authorize]
        [Route("GetTransactionList")]
        public List<AccountTransactionInfo> GetTransactionList()
        {
            return this._service.GetTransactionList();
        }
    }
}