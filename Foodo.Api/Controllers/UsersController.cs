﻿using Foodo.Api.Models;
using Foodo.Core.Common;
using Foodo.Core.Models;
using Foodo.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Foodo.Api.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : BaseController
    {
        private AuthenticationService _service;
        private readonly IConfiguration _config;

        public UsersController(AuthenticationService service, IConfiguration config)
        {
            _service = service;
            _config = config;
        }

        [HttpPost, Authorize]
        [Route("Create")]
        public UserInfo Create([FromBody]CreateUserRequest r)
        {
            return _service.Create(r.Username, r.Password, r.Email);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Login")]
        public UserInfo Login([FromBody]LoginRequest r)
        {
            var result = _service.Login(r.Username, r.Password);

            if (result.Success)
            {
                result.Token = this.GenerateToken(result);
            }

            return result;
        }

        [HttpPost, Authorize]
        [Route("UpdateStatus")]
        public ServiceResponse UpdateStatus()
        {
            return _service.UpdateStatus(this.GetCurrentUserId());

            // Return new status 
        }

        [HttpGet, Authorize]
        [Route("GetList")]
        public List<UserInfo> GetList()
        {
            return _service.GetUserList();
        }

        private string GenerateToken(UserInfo user)
        {
            var claims = new[] {
            new Claim(ClaimTypes.NameIdentifier, user.Username),
            new Claim(ClaimTypes.Email, user.Email),
            new Claim(ClaimTypes.Sid, user.Id.ToString())
        };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(30),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}