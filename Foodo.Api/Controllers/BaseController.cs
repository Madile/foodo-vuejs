﻿using Foodo.Api.Models;
using Foodo.Core.Common;
using Foodo.Core.Entities;
using Foodo.Core.Models;
using Foodo.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Foodo.Api.Controllers
{
    public class BaseController : Controller
    {
        public int GetCurrentUserId()
        {
            if (!HttpContext.User.HasClaim(c => c.Type == ClaimTypes.Sid))
            {
                return -1;
            }

            int userId;
            int.TryParse(HttpContext.User.Claims.FirstOrDefault(p => p.Type == ClaimTypes.Sid).Value, out userId);

            return userId;
        }
    }
}