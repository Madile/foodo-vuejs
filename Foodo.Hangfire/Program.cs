﻿using System.IO;
using Microsoft.Extensions.Configuration;
using Topshelf;

namespace Foodo.Hangfire
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var configuration = builder.Build();

            HostFactory.Run(x =>
            {
                x.StartAutomatically();

                x.Service<HangfireService>(s =>
                {
                    s.ConstructUsing(() => new HangfireService(configuration));
                    s.WhenStarted(service => service.Start());
                    s.WhenStopped(service => service.Stop());
                });

                x.RunAsLocalSystem();
                x.UseAssemblyInfoForServiceInfo();

                x.SetDescription("Hangfire service using IoC and Topshelf");
                x.SetDisplayName("Hangfire foodo");

                // Name can't contain spaces, / or \.
                x.SetServiceName("FoodoHangfire");
            });
        }
    }
}
