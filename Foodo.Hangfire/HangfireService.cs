﻿using System;
using Foodo.Core;
using Foodo.Core.Services;
using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Foodo.Hangfire
{
    public class HangfireService
    {
        private BackgroundJobServer _server;
        private readonly IConfigurationRoot _configuration;
        private readonly string _connectionString;

        public HangfireService(IConfigurationRoot configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("FoodoContext");
        }

        public void Start()
        {
            GlobalConfiguration.Configuration.UseActivator(new FoodoJobActivator(RegisterServices()));
            GlobalConfiguration.Configuration.UseStorage(new PostgreSqlStorage(_connectionString));

            var options = new BackgroundJobServerOptions
            {
                WorkerCount = 1
            };

            _server = new BackgroundJobServer(options);

            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute
            {
                Attempts = 1
            });

            RecurringJob.AddOrUpdate<EventService>(x => x.MarkExpired(), Cron.MinuteInterval(15));
        }

        public void Stop()
        {
            _server.Dispose();
        }

        private ServiceProvider RegisterServices()
        {
            IServiceCollection services = new ServiceCollection();

            services.AddEntityFrameworkNpgsql()
                .AddDbContext<FoodoContext>(options => options.UseNpgsql(_connectionString));

            services.AddScoped<AuthenticationService>();
            services.AddTransient<EventService>();

            return services.BuildServiceProvider();
        }
    }

    public class FoodoJobActivator : JobActivator
    {
        private readonly IServiceProvider _serviceProvider;

        public FoodoJobActivator(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public override object ActivateJob(Type jobType)
        {
            return _serviceProvider.GetService(jobType);
        }
    }
}
