import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import router from './router/index';
import { sync } from 'vuex-router-sync';
import { FontAwesomeIcon } from './icons';
import store from './vuex';
import 'bootstrap';
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.min.css';
import App from 'components/app-root';
import Moment from 'moment';
import 'vue-loaders/dist/vue-loaders.css';
import { BallScaleMultipleLoader } from 'vue-loaders';

// GLOBAL COMPONENTS
Vue.component('icon', FontAwesomeIcon);
Vue.component('vue-ctk-date-time-picker', VueCtkDateTimePicker);
Vue.component(BallScaleMultipleLoader.name, BallScaleMultipleLoader);

// GLOBAL FILTERS
Vue.filter('formatDate', function(value) {
  if (value) {
    return Moment(String(value)).format('DD.MM.YYYY hh:mm');
  }
});

Vue.use(VueAxios, axios);

// If debug else insert production
axios.defaults.baseURL = 'http://localhost:57554/api/';
axios.defaults.headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');

axios.interceptors.response.use((response) => {
  return response;
}, function(error) {
  console.log(error);
  // Do something with response error
  if (error.response.status === 401) {
    store.dispatch('logout');
    router.replace({ path: '/login', redirect: '/login' });
  }
  return Promise.reject(error.response);
});

sync(store, router);

const app = new Vue({
  store,
  router,
  ...App,
  created() {
    // initialize store data structure by submitting action.
  },
  mounted() {
  }
});

export {
  app,
  router,
  store
};
