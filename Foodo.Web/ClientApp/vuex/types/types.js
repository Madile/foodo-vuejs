const key = 'key';
const value = 'value';

const TYPES = {
  USERSTATUSTYPE: {
    ACTIVE: {key: 'Active', value: 1001},
    DISABLED: { key: 'Disabled', value: 1002 }
  },
  EVENTSTATUSTYPE: {
    ACTIVE: { key: 'Active', value: 2001 },
    CLOSED: {key: 'Closed', value: 2002}
  }
};

export default Object.freeze(TYPES);
