import apiHelpers from '../../utils/api';
import apiRoutes from '../../utils/api-routes';
import types from '../../types/types';

const namespaced = true;

// TYPES
const GETUSERLIST_SUCCESS = 'GETUSERLIST_SUCCESS';
const GETUSERLIST_FAILURE = 'GETUSERLIST_FAILURE';
const GETNEWUSER_SUCCESS = 'GETNEWUSER_SUCCESS';
const GETNEWUSER_FAILURE = 'GETNEWUSER_FAILURE';
const ADDNEWUSER_SUCCESS = 'ADDNEWUSER_SUCCESS';
const ADDNEWUSER_FAILURE = 'ADDNEWUSER_FAILURE';
const GETUSERDETAILS_SUCCESS = 'GETUSERDETAILS_SUCCESS';
const GETUSERDETAILS_FAILURE = 'GETUSERDETAILS_FAILURE';
const UPDATEUSERSTATUS_SUCCESS = 'UPDATEUSERSTATUS_SUCCESS';
const UPDATEUSERSTATUS_FAILURE = 'UPDATEUSERSTATUS_FAILURE';

// STATE
const state = {
  success: '',
  error: '',
  selectedUser: '',
  users: ''
};

// MUTATIONS
const mutations = {
  [GETUSERLIST_SUCCESS](state, payload) {
    state.users = payload;
  },

  [GETUSERLIST_FAILURE](state, payload) {
    state.error = payload;
  },

  [GETNEWUSER_SUCCESS](state, payload) {

  },

  [GETNEWUSER_FAILURE](state, payload) {
    state.error = payload;
  },

  [ADDNEWUSER_SUCCESS](state, payload) {
    state.users.push(payload);
    state.success = 'User added succesfully';
  },

  [ADDNEWUSER_FAILURE](state, payload) {
    state.error = payload;
  },

  [GETUSERDETAILS_SUCCESS](state, payload) {
    state.selectedUser = payload;
  },

  [GETUSERDETAILS_FAILURE](state, payload) {
    state.error = payload;
  },

  [UPDATEUSERSTATUS_SUCCESS](state, payload) {
    // UPDATE USER STATUS
    //var i = state.users.findIndex(obj => obj.username === state.selectedUser.username);
    //state.users[i].status = payload;
    // TODO: Return new status and update state
    state.success = 'User status succesfully updated.';
  },

  [UPDATEUSERSTATUS_FAILURE](state, payload) {
    state.error = payload;
  }
};

// ACTIONS
const actions = {
  getUserList({ commit }, payload) {
    return apiHelpers.get(apiRoutes.GET_USER_LIST)
      .then((response) => commit(GETUSERLIST_SUCCESS, response.data))
      .catch((error) => {
        state.error = error;
        commit(GETUSERLIST_FAILURE, error);
      });
  },

  newUser({ commit }, payload) {
    commit(GETNEWUSER_SUCCESS, payload);
  },

  addNewUser({ commit }, payload) {
    return apiHelpers.post(apiRoutes.ADD_USER, { username: payload.username, password: payload.password, email: payload.email })
      .then((response) => commit(ADDNEWUSER_SUCCESS, response.data))
      .catch((error) => {
        state.error = error;
        commit(ADDNEWUSER_FAILURE, error);
      });
  },

  getUserDetails({ commit }, payload) {
    commit(GETUSERDETAILS_SUCCESS, payload);
  },

  updateStatus({ commit }, payload) {
    return apiHelpers.post(apiRoutes.UPDATE_USER_STATUS, { username: payload })
      .then((response) => commit(UPDATEUSERSTATUS_SUCCESS, payload))
      .catch((error) => {
        state.error = error;
        commit(UPDATEUSERSTATUS_FAILURE, error);
      });
  }
};

const getters = {
  userList: state => state.users,
  selectedUser: state => state.selectedUser
};

export default {
  state,
  mutations,
  actions,
  getters
};
