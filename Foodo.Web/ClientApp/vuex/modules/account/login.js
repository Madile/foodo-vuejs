import apiHelpers from '../../utils/api';
import apiRoutes from '../../utils/api-routes';
import actionNames from '../../utils/action-names';
const namespaced = true;

// TYPES
const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const LOGIN_FAILURE = 'LOGIN_FAILURE';
const LOGIN_ERROR = 'LOGIN_ERROR';
const LOGOUT = 'LOGOUT';

// STATE
const state = {
  isLoggedIn: !!localStorage.getItem('token'),
  username: localStorage.getItem('username'),
  error: ''
};

// MUTATIONS
const mutations = {
  [LOGIN_SUCCESS](state, payload) {
    localStorage.setItem('token', payload.token);
    localStorage.setItem('username', payload.username);
    state.username = payload.username;
    state.isLoggedIn = true;
  },
  [LOGIN_FAILURE](state, error) {
    state.error = error;
  },
  [LOGIN_ERROR](state, error) {
    state.error = error;
  },
  [LOGOUT](state) {
    state.isLoggedIn = false;
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    state.username = '';
  }
};

const getters = {
  username: state => state.username,
  isLoggedIn: state => state.isLoggedIn
};

const actions = {
  login({ dispatch, commit }, payload) {
    dispatch(actionNames.STARTLOADING, { root: true });
    return apiHelpers.post(apiRoutes.LOGIN, { username: payload.username, password: payload.password })
      .then((response) => {
        if (response.data.success) {
          commit(LOGIN_SUCCESS, response.data);
        } else {
          commit(LOGIN_FAILURE, response.data.message);
        }
      })
      .catch((error) => {
        commit(LOGIN_ERROR, error);
      })
      .finally((resolve) => dispatch(actionNames.ENDLOADING, { root: true }));
  },
  logout({ commit }) {
    commit(LOGOUT);
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
