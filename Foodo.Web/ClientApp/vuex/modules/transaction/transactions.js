import apiHelpers from '../../utils/api';
import apiRoutes from '../../utils/api-routes';

// TYPES
const GETTRANSACTIONSLIST_SUCCESS = 'GETTRANSACTIONSLIST_SUCCESS';
const GETTRANSACTIONLIST_FAILURE = 'GETTRANSACTIONLIST_FAILURE';

const namespaced = true;

// STATE
const state = {
  transactions: '',
  error: '',
  success: ''
};

// MUTATIONS
const mutations = {
  [GETTRANSACTIONSLIST_SUCCESS](state, payload) {
    state.transactions = payload;
  },
  [GETTRANSACTIONLIST_FAILURE](state, payload) {
    state.error = payload;
  }
};

// ACTIONS
const actions = {
  getTransactionList({ commit }, payload) {
    return apiHelpers.get(apiRoutes.GET_TRANSACTION_LIST)
      .then((response) => {
        if (response.data) {
          commit(GETTRANSACTIONSLIST_SUCCESS, response.data);
        } else {
          commit(GETTRANSACTIONLIST_FAILURE, response.data.message);
        }
      })
      .catch((error) => commit(GETTRANSACTIONLIST_FAILURE, error));
  }
};

const getters = {
  allTransactions: state => state.transactions
};

export default {
  state,
  mutations,
  actions,
  getters
};
