import apiRoutes from '../utils/api-routes';
import jquery from 'jquery';
import actionNames from '../utils/action-names';
const namespaced = true;

// TYPES
const LOADING_START = 'LOGIN_START';
const LOADING_END = 'LOGIN_END';
const NOFITY_SLACK_SUCCESS = 'NOTIFY_SLACK_SUCCESS';
const NOTIFY_SLACK_FAILURE = 'NOTIFY_SLACK_FAILURE';

// STATE
const state = {
  loading: false
};

// MUTATIONS
const mutations = {
  [LOADING_START](state, payload) {
    state.loading = true;
  },
  [LOADING_END](state, payload) {
    state.loading = false;
  },
  [NOFITY_SLACK_SUCCESS](state, payload) {

  },
  [NOTIFY_SLACK_FAILURE](state, payload) {

  }
};

const getters = {
  loading: state => state.loading
};

const actions = {
  notifySlack({ commit }, payload) {
    let test = { 'text': payload };
    test = JSON.stringify(test);
    jquery.ajax({
      url: apiRoutes.SLACK_NOFITICATION,
      type: 'POST',
      dataType: 'json',
      data: test
    });
  },
  [actionNames.STARTLOADING]({ commit }, payload) {
    commit(LOADING_START);
  },
  [actionNames.ENDLOADING]({ commit }, payload) {
    commit(LOADING_END);
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
