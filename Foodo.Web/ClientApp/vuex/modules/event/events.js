import apiHelpers from '../../utils/api';
import apiRoutes from '../../utils/api-routes';
import actionNames from '../../utils/action-names';
// Make module namespaced
const namespaced = true;

// TYPES
const GETEVENTLIST_SUCCESS = 'GETEVENTLIST';
const GETEVENTLIST_FAILURE = 'GETEVENTLIST_FAILURE';
const GETNEWEVENT_SUCCESS = 'GETNEWEVENT_SUCCESS';
const ADDNEWEVENT_SUCCESS = 'ADDNEWEVENT_SUCCESS';
const ADDNEWEVENT_FAILURE = 'ADDNEWEVENT_FAILURE';
const GETEVENTDETAILS_SUCCESS = 'GETEVENTDETAILS_SUCCESS';
const GETEVENTDETAILS_FAILURE = 'GETEVENTDETAILS_FAILURE';
const SUBSCRIBE_SUCCESS = 'SUBSCRIBE_SUCCESS';
const SUBSCRIBE_FAILURE = 'SUBSCRIBE_FAILURE';
const SUBSCRIBE_REDIRECT_SUCCESS = 'SUBSCRIBE_REDIRECT_SUCCESS';
const SUBSCRIBE_REDIRECT_FAILURE = 'SUBSCRIBE_REDIRECT_FAILURE';
const CLOSEEVENT_SUCCESS = 'CLOSEEVENT_SUCCESS';
const CLOSEEVENT_FAILURE = 'CLOSEEVENT_FAILURE';
const GETACTIVEEVENTS_SUCCESS = 'GETACTIVEEVENTS_SUCCESS';
const GETACTIVEEVENTS_FAILURE = 'GETACTIVEEVENTS_FAILURE';

// STATE
const state = {
  error: '',
  success: '',
  events: '',
  selectedEvent: '',
  subscribeToEventId: '',
  activeTransactions: ''
};

const getters = {
  allEvents: state => state.events,
  selectedEvent: state => state.selectedEvent,
  selectedEventSubscribers: state => state.selectedEvent.subscribers,
  success: state => state.success,
  activeEvents: state => state.activeTransactions
};

// MUTATIONS
const mutations = {
  [GETEVENTLIST_SUCCESS](state, payload) {
    state.events = payload;
  },
  [GETEVENTLIST_FAILURE](state, error) {
    state.error = error.message;
  },
  [GETNEWEVENT_SUCCESS](state, payload) {

  },
  [ADDNEWEVENT_SUCCESS](state, payload) {
    state.events.push(payload);
  },
  [ADDNEWEVENT_FAILURE](state, error) {
    state.error = error;
  },
  [GETEVENTDETAILS_SUCCESS](state, payload) {
    state.selectedEvent = payload;
  },
  [GETEVENTDETAILS_FAILURE](state, payload) {
    state.error = payload.message;
  },
  [SUBSCRIBE_SUCCESS](state, payload) {
    state.selectedEvent.eventItems.push(payload);
  },
  [SUBSCRIBE_FAILURE](state, payload) {
    state.error = payload.message;
  },
  [SUBSCRIBE_REDIRECT_SUCCESS](state, payload) {
    state.subscribeToEventId = payload;
  },
  [SUBSCRIBE_REDIRECT_FAILURE](state, error) {
    state.error = error.message;
  },
  [CLOSEEVENT_SUCCESS](state, payload) {
    state.selectedEvent.eventStatus = 'Closed';
  },
  [CLOSEEVENT_FAILURE](state, payload) {
    state.error = payload;
  },
  [GETACTIVEEVENTS_SUCCESS](state, payload) {
    state.activeTransactions = payload;
  },
  [GETACTIVEEVENTS_FAILURE](state, payload) {
    state.error = payload;
  }
};

// ACTIONS
const actions = {
  [actionNames.GETEVENTLIST]({ commit }, payload) {
    return apiHelpers.get(apiRoutes.GET_EVENT_LIST)
      .then((response) => commit(GETEVENTLIST_SUCCESS, response.data))
      .catch((error) => commit(GETEVENTLIST_FAILURE, error));
  },

  newEvent({ commit }, payload) {
    commit(GETNEWEVENT_SUCCESS, payload);
  },

  eventDetails({ commit }, payload) {
    commit(GETEVENTDETAILS_SUCCESS, payload);
  },

  addNewEvent({ dispatch, commit }, payload) {
    dispatch(actionNames.STARTLOADING, { root: true });
    return apiHelpers.post(apiRoutes.ADD_EVENT,
      {
        EventTitle: payload.eventTitle,
        EventDescription: payload.eventDescription,
        ValidUntil: payload.validUntil,
        categoryId: payload.categoryId,
        createdUsername: payload.createdUsername
      })
      .then((response) => {
        if (response.data.success) {
          commit(ADDNEWEVENT_SUCCESS, response.data.data);
          dispatch(actionNames.NOTIFYSLACK, 'New event created', { root: true });
        } else {
          commit(ADDNEWEVENT_FAILURE, response.data.message);
        }
      })
      .catch((error) => commit(ADDNEWEVENT_FAILURE, error))
      .finally((resolve) => dispatch(actionNames.ENDLOADING, { root: true }));
  },

  subscription({ commit }, payload) {
    commit(SUBSCRIBE_REDIRECT_SUCCESS, payload.id);
  },

  subscribe({ dispatch, commit }, payload) {
    dispatch(actionNames.STARTLOADING, { root: true });
    return apiHelpers.post(apiRoutes.SUBSCRIBE_TO_EVENT,
      {
        Description: payload.description,
        Amount: parseFloat(payload.amount),
        EventId: parseInt(state.subscribeToEventId)
      })
      .then((response) => {
        if (response.data.success) {
          commit(SUBSCRIBE_SUCCESS, payload);
        } else {
          commit(SUBSCRIBE_FAILURE, response.data.message);
        }
      })
      .catch((error) => commit(SUBSCRIBE_FAILURE, error))
      .finally((resolve) => dispatch(actionNames.ENDLOADING, { root: true }));
  },

  closeEvent({ dispatch, commit }, payload) {
    dispatch(actionNames.STARTLOADING, { root: true });
    return apiHelpers.post(apiRoutes.CLOSE_EVENT, { EventId: parseInt(state.selectedEvent.id) })
      .then((response) => {
        if (response.data.success) {
          commit(CLOSEEVENT_SUCCESS, response.data.message);
          dispatch(actionNames.NOTIFYSLACK, 'Event closed', { root: true });
        } else {
          commit(CLOSEEVENT_FAILURE, response.data.message);
        }
      })
      .catch((error) => commit(CLOSEEVENT_FAILURE, error))
      .finally((resolve) => dispatch(actionNames.ENDLOADING, { root: true }));
  },

  [actionNames.GETACTIVEEVENTS]({ commit }, payload) {
    var filter = [];
    for (var i = 0; i < state.events.length; i++) {
      if (state.events[i].eventStatus === 'Active') {
        filter.push(state.events[i]);
      }
    }

    commit(GETACTIVEEVENTS_SUCCESS, filter);
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
