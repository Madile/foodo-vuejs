import Vue from 'vue';
import Vuex from 'vuex';

// Modules
import loginModule from './modules/account/login';
import eventListModule from './modules/event/events';
import transactionListModule from './modules/transaction/transactions';
import userModule from './modules/user/users';
import commonModule from './modules/common';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    login: loginModule,
    eventList: eventListModule,
    transactionList: transactionListModule,
    user: userModule,
    common: commonModule
  }
});
