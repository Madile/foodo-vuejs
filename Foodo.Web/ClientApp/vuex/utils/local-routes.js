const LOCAL_ROUTES = {
  EVENT_LIST: '/events',
  LOGIN: '/login',
  INDEX: '/',
  NEW_EVENT: '/new-event',
  EVENT_DETAILS: '/event-details',
  TRANSACTIONS: '/transactions',
  NEW_USER: '/new-user',
  SUCCESS: '/success',
  ERROR: '/error',
  USER_LIST: '/users',
  USER_DETAILS: '/user-details',
  SUBSCRIBE_TO_EVENT: '/subscribe'
};

export default Object.freeze(LOCAL_ROUTES);
