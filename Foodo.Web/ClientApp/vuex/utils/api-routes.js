const API_ROUTES = {
  ADD_USER: 'Users/Create',
  UPDATE_USER_STATUS: 'Users/UpdateStatus',
  GET_USER_LIST: 'Users/GetList',
  LOGIN: 'Users/Login',
  ADD_EVENT: 'Events/Create',
  GET_EVENT_LIST: 'Events/GetList',
  SUBSCRIBE_TO_EVENT: 'Events/Subscribe',
  CLOSE_EVENT: 'Events/Close',
  GET_TRANSACTION_LIST: 'Events/GetTransactionList',
  SLACK_NOFITICATION: 'https://hooks.slack.com/services/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'
};

export default Object.freeze(API_ROUTES);
