import axios from 'axios';

export default {
  get(url, request) {
    return axios.get(url, request, {
      'Content-Type': 'application/json'
    })
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error));
  },

  post(url, request) {
    return axios.post(url, request, {
      'Content-Type': 'application/json'
    })
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error));
  }
};
