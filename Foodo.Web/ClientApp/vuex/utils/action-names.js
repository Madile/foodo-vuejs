const ACTIONS = {
  GETEVENTLIST: 'getEventList',
  EVENTDETAILS: 'eventDetails',
  NEWEVENT: 'newEvent',
  ADDNEWEVENT: 'addNewEvent',
  SUBSCRIPTION: 'subscription',
  GETTRANSACTIONLIST: 'getTransactionList',
  GETUSERLIST: 'getUserList',
  USERDETAILS: 'getUserDetails',
  NEWUSER: 'newUser',
  UPDATEUSERSTATUS: 'updateStatus',
  SUBSCRIBETOEVENT: 'subscribe',
  LOGOUT: 'logout',
  LOGIN: 'login',
  CLOSEEVENT: 'closeEvent',
  STARTLOADING: 'startLoading',
  ENDLOADING: 'endLoading',
  GETACTIVEEVENTS: 'getActiveEvents',
  NOTIFYSLACK: 'notifySlack'
};

export default Object.freeze(ACTIONS);
