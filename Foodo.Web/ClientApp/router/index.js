import Vue from 'vue';
import VueRouter from 'vue-router';
import { routes } from './routes';
import store from '../vuex/index';
import localRoutes from '../vuex/utils/local-routes';

Vue.use(VueRouter);

let router = new VueRouter({
  mode: 'history',
  routes
});

if (!store.state.login.isLoggedIn) {
  router.replace({ path: '/login', redirect: '/login' });
} else {
  router.replace({ path: '/', redirect: '/' });
}

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const authRequired = ![localRoutes.LOGIN].includes(to.path);
  const loggedIn = localStorage.getItem('token');

  if (authRequired && !loggedIn) {
    return next(localRoutes.LOGIN);
  }

  next();
});

export default router;
