import HomePage from 'components/home-page';
import Login from 'components/login';
import EventsList from 'components/event-list';
import NewEvent from 'components/new-event';
import TransactionsList from 'components/transaction-list';
import NewUser from 'components/new-user';
import EventDetails from 'components/event-details';
import Success from 'components/success';
import UserList from 'components/user-list';
import UserDetails from 'components/user-details';
import Subscribe from 'components/subscribe';

import localRoutes from '../vuex/utils/local-routes';

export const routes = [
  // MENU VIEWS
  { name: 'home', path: localRoutes.INDEX, component: HomePage, display: 'Home', icon: 'home', allowDisplay: true },
  { name: 'eventsList', path: localRoutes.EVENT_LIST, component: EventsList, display: 'Events', icon: 'list', allowDisplay: true },
  { name: 'transactionList', path: localRoutes.TRANSACTIONS, component: TransactionsList, display: 'Transactions', icon: 'list', allowDisplay: true },
  { name: 'userList', path: localRoutes.USER_LIST, component: UserList, icon: 'user', display: 'Users', allowDisplay: true },

  // SUB VIEWS
  { name: 'login', path: localRoutes.LOGIN, component: Login, display: 'Login', allowDisplay: false },
  { name: 'newEvent', path: localRoutes.NEW_EVENT, component: NewEvent, display: 'New event', allowDisplay: false },
  { name: 'eventDetails', path: localRoutes.EVENT_DETAILS, component: EventDetails, display: 'Event details', allowDisplay: false },
  { name: 'newUser', path: localRoutes.NEW_USER, component: NewUser, display: 'New user', allowDisplay: false },
  { name: 'success', path: localRoutes.SUCCESS, component: Success, display: 'Success', allowDisplay: false },
  { name: 'userDetails', path: localRoutes.USER_DETAILS, component: UserDetails, display: 'UserDetails', allowDisplay: false },
  { name: 'subscribe', path: localRoutes.SUBSCRIBE_TO_EVENT, component: Subscribe, display: 'UserDetails', allowDisplay: false }
];
