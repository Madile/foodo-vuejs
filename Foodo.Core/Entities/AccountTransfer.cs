﻿using System;

namespace Foodo.Core.Entities
{
    /// <summary>
    /// This entity represents deposits and withdrawals, one way transactions.
    /// </summary>
    public class AccountTransfer : EntityBase
    {
        public int CreatedUserId { get; set; }
        public int? ConfirmedUserId { get; set; }
        public int? TransactionId { get; set; }
        public AccountTransactionStatusEnum Status { get; set; }
        public DateTime TimeCreated { get; set; }
        public DateTime? TimeConfirmed { get; set; }
        public decimal Amount { get; set; }

        public virtual User CreatedUser { get; set; }
        public virtual User ConfirmedUser { get; set; }
        public virtual AccountTransaction Transaction { get; set; }
    }
}
