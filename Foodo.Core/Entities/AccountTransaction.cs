﻿using System;

namespace Foodo.Core.Entities
{
    public class AccountTransaction : EntityBase
    {
        public AccountTransactionSourceTypeEnum SourceType { get; set; }
        public AccountTransactionStatusEnum Status { get; set; }
        public AccountTransactionTypeEnum Type { get; set; }
        public DateTime TimeCreated { get; set; }
        public decimal Amount { get; set; }
    }
}
