﻿using System.ComponentModel.DataAnnotations;

namespace Foodo.Core.Entities
{
    public class EntityBase
    {
        [Key]
        public virtual int Id { get; set; }
    }
}
