﻿using System;

namespace Foodo.Core.Entities
{
    /// <summary>
    /// This entity represents C2C transactions, which should be 0-sum actions.
    /// </summary>
    public class UserTransfer : EntityBase
    {
        public int SenderUserId { get; set; }
        public int ReceiverUserId { get; set; }
        public int SendTransactionId { get; set; }
        public int ReceiveTransactionId { get; set; }
        public DateTime TimeCreated { get; set; }
        public decimal Amount { get; set; }

        public virtual User SenderUser { get; set; }
        public virtual User ReceiverUser { get; set; }
        public virtual AccountTransaction SendTransaction { get; set; }
        public virtual AccountTransaction ReceiveTransaction { get; set; }
    }
}
