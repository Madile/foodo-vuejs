﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Foodo.Core.Entities
{
    public class Account : EntityBase
    {
        public Account()
        {
            this.Transactions = new HashSet<AccountTransaction>();
        }

        [ForeignKey(nameof(Account.User))]
        public override int Id { get; set; }
        public decimal Balance { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<AccountTransaction> Transactions { get; set; }
    }
}
