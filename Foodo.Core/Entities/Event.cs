﻿using System;
using System.Collections.Generic;

namespace Foodo.Core.Entities
{
    public class Event : EntityBase
    {
        public Event()
        {
            this.Items = new HashSet<EventItem>();
        }

        public int CreatedUserId { get; set; }
        public EventStatusEnum Status { get; set; }
        public EventCategoryEnum Category { get; set; }
        public DateTime TimeCreated { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Amount { get; set; }
        public decimal Tip { get; set; }
        public string EventTitle { get; set; }
        public string EventDescription { get; set; }
        public DateTime ValidUntil { get; set; }

        public virtual User CreatedUser { get; set; }
        public virtual ICollection<EventItem> Items { get; set; }
    }
}
