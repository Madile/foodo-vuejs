﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Foodo.Core.Entities
{
    public class User : EntityBase
    {
        public User()
        {
            this.Items = new HashSet<EventItem>();
        }

        public UserStatusEnum Status { get; set; }
        public string Username { get; set; }
        public byte[] Password { get; set; }
        public string Email { get; set; }
        public DateTime TimeCreated { get; set; }

        public virtual Account Account { get; set; }
        public virtual ICollection<EventItem> Items { get; set; }
    }
}
