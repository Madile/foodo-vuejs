﻿namespace Foodo.Core.Entities
{
    public class EventItem : EntityBase
    {
        public int EventId { get; set; }
        public int TransactionId { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }

        public virtual Event Event { get; set; }
        public virtual User User { get; set; }
        public virtual AccountTransaction Transaction { get; set; }
    }
}
