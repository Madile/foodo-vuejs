﻿using Foodo.Core.Common;

namespace Foodo.Core
{
    public enum TypeItemEnumSeries
    {
        UserStatusEnum = 1000,
        AccountTransactionStatusEnum = 1100,
        AccountTransactionTypeEnum = 1200,
        EventCategoryEnum = 1300,
        EventStatusEnum = 1400,
        AccountTransferStatusEnum = 1500,
        AccountTransactionSourceTypeEnum = 1600
    }

    public enum UserStatusEnum
    {
        Active = TypeItemEnumSeries.UserStatusEnum + 1,
        Disabled = TypeItemEnumSeries.UserStatusEnum + 2
    }

    public enum AccountTransactionStatusEnum
    {
        Confirmed = TypeItemEnumSeries.AccountTransactionStatusEnum + 1,
    }

    public enum AccountTransactionTypeEnum
    {
        Deposit = TypeItemEnumSeries.AccountTransactionTypeEnum + 1,
        Withdrawal = TypeItemEnumSeries.AccountTransactionTypeEnum + 2,
        Payment = TypeItemEnumSeries.AccountTransactionSourceTypeEnum + 3
    }
    
    public enum EventCategoryEnum
    {
        Food = TypeItemEnumSeries.EventCategoryEnum + 1,
        Recreation = TypeItemEnumSeries.EventCategoryEnum + 2,
        Other = TypeItemEnumSeries.EventCategoryEnum + 3
    }

    public enum EventStatusEnum
    {
        Active = TypeItemEnumSeries.EventStatusEnum + 1,
        Expired = TypeItemEnumSeries.EventStatusEnum + 2,
        Closed = TypeItemEnumSeries.EventStatusEnum + 3
    }

    public enum AccountTransferStatusEnum
    {
        Requested = TypeItemEnumSeries.AccountTransferStatusEnum + 1,
        Confirmed = TypeItemEnumSeries.AccountTransferStatusEnum + 2
    }

    public enum AccountTransactionSourceTypeEnum
    {
        AccountTransfer = TypeItemEnumSeries.AccountTransactionSourceTypeEnum + 1,
        UserTransferSend = TypeItemEnumSeries.AccountTransactionSourceTypeEnum + 2,
        UserTransferReceive = TypeItemEnumSeries.AccountTransactionSourceTypeEnum + 3,
        EventItem = TypeItemEnumSeries.AccountTransactionSourceTypeEnum + 4
    }
}
