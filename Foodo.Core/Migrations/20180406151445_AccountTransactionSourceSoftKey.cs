﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Foodo.Core.Migrations
{
    public partial class AccountTransactionSourceSoftKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SourceId",
                table: "AccountTransactions",
                nullable: false);

            migrationBuilder.AddColumn<int>(
                name: "SourceType",
                table: "AccountTransactions",
                nullable: false);

            migrationBuilder.CreateIndex(
                name: "IX_AccountTransactions_SourceType_SourceId",
                table: "AccountTransactions",
                columns: new[] { "SourceType", "SourceId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AccountTransactions_SourceType_SourceId",
                table: "AccountTransactions");

            migrationBuilder.DropColumn(
                name: "SourceId",
                table: "AccountTransactions");

            migrationBuilder.DropColumn(
                name: "SourceType",
                table: "AccountTransactions");
        }
    }
}
