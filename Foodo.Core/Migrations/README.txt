﻿UPDATE:
Update-Database -Context FoodoContext -Project Foodo.Core -StartupProject Foodo.Api

ADD MIGRATION:
Add-Migration -Context FoodoContext -Project Foodo.Core -StartupProject Foodo.Api -OutputDir Migrations [MIGRATIONNAME]