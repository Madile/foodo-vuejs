﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Foodo.Core.Migrations
{
    public partial class Changes_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AccountTransactions_SourceType",
                table: "AccountTransactions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_AccountTransactions_SourceType",
                table: "AccountTransactions",
                column: "SourceType",
                unique: true);
        }
    }
}
