﻿namespace Foodo.Core.Common
{
    public class ServiceResponse
    {
        public bool Success { get; set; }
        public int ResponseCode { get; set; }
        public string Message { get; set; }
    }

    public class ServiceResponse<T> : ServiceResponse
    {
        public T Data { get; set; }
    }
}