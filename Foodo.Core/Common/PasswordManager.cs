﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Foodo.Core.Common
{
    public static class PasswordManager
    {
        public const int SaltLength = 16;

        /// <summary>
        /// Checks if cleartext password after hashing is same as hashed password.
        /// </summary>
        /// <param name="password">"cleartext" password</param>
        /// <param name="passwordHash">hash to comare password</param>
        /// <returns></returns>
        public static bool ComparePasswordWithHash(string password, byte[] passwordHash)
        {
            if (string.IsNullOrEmpty(password) || passwordHash == null)
            {
                return false;
            }

            return CompareHash(Encoding.UTF8.GetBytes(password), passwordHash);
        }

        /// <summary>
        /// Returns cripted password from string
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static byte[] EncryptPassword(string password)
        {
            byte[] salt = GetRandomBytes(SaltLength);
            byte[] pass = Encoding.UTF8.GetBytes(password);
            AddSaltToPlainText(ref salt, ref pass);
            byte[] hash = ComputeHash(pass);
            return CombineBytes(salt, hash);
        }

        #region Password check from Microsoft Enterprise Library

        public static bool CompareHash(byte[] plaintext, byte[] hashedtext)
        {
            if (plaintext == null) throw new ArgumentNullException("plainText");
            if (hashedtext == null) throw new ArgumentNullException("hashedText");
            if (hashedtext.Length == 0) throw new ArgumentException("HashedText lenght must be at least 1!");

            bool result = false;
            byte[] hashedPlainText = null;
            byte[] salt = null;

            try
            {
                salt = ExtractSalt(hashedtext);
                hashedPlainText = CreateHashWithSalt(plaintext, salt);
            }
            finally
            {
                ZeroOutBytes(salt);
            }
            result = CompareBytes(hashedPlainText, hashedtext);
            return result;
        }

        public static bool CompareBytes(byte[] byte1, byte[] byte2)
        {
            if (byte1 == null || byte2 == null)
            {
                return false;
            }
            if (byte1.Length != byte2.Length)
            {
                return false;
            }

            bool result = true;
            for (int i = 0; i < byte1.Length; i++)
            {
                if (byte1[i] != byte2[i])
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        private static byte[] ExtractSalt(byte[] hashedtext)
        {
            byte[] salt = null;
            if (hashedtext.Length > SaltLength)
            {
                salt = new byte[SaltLength];
                Buffer.BlockCopy(hashedtext, 0, salt, 0, SaltLength);
            }
            return salt;
        }

        public static void ZeroOutBytes(byte[] bytes)
        {
            if (bytes == null)
            {
                return;
            }
            Array.Clear(bytes, 0, bytes.Length);
        }

        private static byte[] CreateHashWithSalt(byte[] plaintext, byte[] salt)
        {
            AddSaltToPlainText(ref salt, ref plaintext);
            byte[] hash = ComputeHash(plaintext);
            hash = CombineBytes(salt, hash);
            return hash;
        }

        public static byte[] CombineBytes(byte[] buffer1, byte[] buffer2)
        {
            byte[] combinedBytes = new byte[buffer1.Length + buffer2.Length];
            Buffer.BlockCopy(buffer1, 0, combinedBytes, 0, buffer1.Length);
            Buffer.BlockCopy(buffer2, 0, combinedBytes, buffer1.Length, buffer2.Length);

            return combinedBytes;
        }

        public static byte[] ComputeHash(byte[] plaintext)
        {
            byte[] hash = null;

            using (var algorithm = new SHA256Managed())
            {
                hash = algorithm.ComputeHash(plaintext);
            }

            return hash;
        }

        private static void AddSaltToPlainText(ref byte[] salt, ref byte[] plaintext)
        {
            if (salt == null)
            {
                salt = GetRandomBytes(SaltLength);
            }

            plaintext = CombineBytes(salt, plaintext);
        }

        public static byte[] GetRandomBytes(int size)
        {
            byte[] randomBytes = new byte[size];
            GetRandomBytes(randomBytes);
            return randomBytes;
        }

        public static void GetRandomBytes(byte[] bytes)
        {
            RNGCryptoServiceProvider.Create().GetBytes(bytes);
        }

        #endregion Password check from Microsoft Enterprise Library
    }
}