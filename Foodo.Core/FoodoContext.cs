﻿using Foodo.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Foodo.Core
{
    public class FoodoContext : DbContext
    {
        public FoodoContext(DbContextOptions<FoodoContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountTransaction> AccountTransactions { get; set; }
        public DbSet<AccountTransfer> AccountTransfers { get; set; }
        public DbSet<UserTransfer> UserTransfers { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventItem> EventItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
