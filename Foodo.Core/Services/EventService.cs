﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foodo.Core.Common;
using Foodo.Core.Entities;
using Foodo.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Foodo.Core.Services
{
    public class EventService : ServiceBase
    {
        public EventService(FoodoContext context) : base(context)
        {
        }

        public ServiceResponse Create(string title, string description, DateTime validUntil, int categoryId, int userId)
        {
            var user = Context.Users.FirstOrDefault(p => p.Id == userId);
            var newEvent = new Event
            {
                CreatedUser = user,
                EventDescription = description,
                EventTitle = title,
                Status = EventStatusEnum.Active,
                Category = (EventCategoryEnum)categoryId,
                ValidUntil = validUntil,
                TimeCreated = DateTime.Now
            };

            Context.Events.Add(newEvent);
            Context.SaveChanges();

            var newEventInfo = new EventInfo
            {
                CreatedUsername = user.Username,
                EventCategory = newEvent.Category,
                EventDescription = newEvent.EventDescription,
                EventStatus = newEvent.Status,
                EventTitle = newEvent.EventTitle,
                ValidUntil = newEvent.ValidUntil,
                TimeCreated = newEvent.TimeCreated
            };

            return Success<EventInfo>(newEventInfo);
        }

        public List<EventInfo> GetList()
        {
           var events = Context.Events.Include(p => p.Items).ThenInclude(m => m.User).ToList();

            return events.Select(a => new EventInfo()
            {
                CreatedUsername = a.CreatedUser?.Username,
                EventCategory = a.Category,
                EventDescription = a.EventDescription,
                EventStatus = a.Status,
                EventTitle = a.EventTitle,
                TimeCreated = a.TimeCreated,
                ValidUntil = a.ValidUntil,
                Id = a.Id,
                EventItems = a.Items.Select(p => new EventItemInfo() {
                    Amount = p.Amount,
                    Description = p.Description,
                    Username = p.User?.Username
                }).ToList()
            })
               .ToList();
        }

        public ServiceResponse Subscribe(int eventId, string description, int userId, decimal amount)
        {
            var e = Context.Events
                .FirstOrDefault(m => m.Id == eventId);

            e.Items.Add(new EventItem
            {
                Amount = amount,
                Description = description,
                UserId = userId,
                EventId = eventId,
                Transaction = new AccountTransaction {
                    Type = AccountTransactionTypeEnum.Payment,
                    TimeCreated = DateTime.Now,
                    Status = AccountTransactionStatusEnum.Confirmed,
                    SourceType = AccountTransactionSourceTypeEnum.EventItem,
                    Amount = amount                   
                }
            });

            Context.SaveChanges();

            return Success();
        }

        public ServiceResponse Close(int eventId)
        {
            var e = this.Context.Events.FirstOrDefault(p => p.Id == eventId);

            e.Status = EventStatusEnum.Closed;

            this.Context.SaveChanges();

            return Success("Event succesfully closed");
        }

        public List<AccountTransactionInfo> GetTransactionList()
        {
            var at = this.Context.AccountTransactions.ToList();

            return at.Select(p => new AccountTransactionInfo
            {
                Amount = p.Amount,
                SourceType = p.SourceType,
                Status = p.Status,
                TimeCreated = p.TimeCreated,
                Type = p.Type,
            }).ToList();
        }

        public void MarkExpired()
        {
            var expiredEvents = Context.Events
                .Where(x => x.ValidUntil < DateTime.Now && x.Status == EventStatusEnum.Active)
                .ToList();

            foreach (var expiredEvent in expiredEvents)
            {
                expiredEvent.Status = EventStatusEnum.Expired;
            }

            Context.SaveChanges();
        }
    }
}
