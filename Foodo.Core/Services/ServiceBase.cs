﻿using Foodo.Core.Common;

namespace Foodo.Core.Services
{
    public abstract class ServiceBase
    {
        protected FoodoContext Context { get; }

        protected ServiceBase(FoodoContext context)
        {
            this.Context = context;
        }

        protected ServiceResponse Failure(string message)
        {
            return new ServiceResponse
            {
                Message = message,
                Success = false
            };
        }

        protected ServiceResponse Failure<T>(string message)
        {
            return new ServiceResponse<T>
            {
                Message = message,
                Success = false,
                Data = default(T)
            };
        }

        protected ServiceResponse Success()
        {
            return new ServiceResponse
            {
                Success = true
            };
        }

        protected ServiceResponse Success<T>(T data)
        {
            return new ServiceResponse<T>
            {
                Success = true,
                Data = data
            };
        }
    }
}
