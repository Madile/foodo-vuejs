﻿using Foodo.Core.Common;
using Foodo.Core.Entities;
using Foodo.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Foodo.Core.Services
{
    public class AuthenticationService : ServiceBase
    {
        public AuthenticationService(FoodoContext context) : base(context)
        {
        }

        public UserInfo Login(string username, string password)
        {
            var result = new UserInfo();

            var user = Context.Users
                .Where(m => m.Username == username)
                .FirstOrDefault();

            if (!PasswordManager.ComparePasswordWithHash(password, user.Password))
            {
                result.Message = "Invalid pass";
                return result;
            }

            result.Email = user.Email;
            result.Status = user.Status;
            result.TimeCreated = user.TimeCreated;
            result.Username = user.Username;
            result.Id = user.Id;

            result.Success = true;
            result.ResponseCode = 200;

            return result;
        }

        public ServiceResponse UpdateStatus(int userId)
        {
            var user = Context.Users
                .FirstOrDefault(p => p.Id == userId);

            if (user == null)
            {
                return Failure("User not found");
            }

            user.Status = user.Status == UserStatusEnum.Active ? UserStatusEnum.Disabled : UserStatusEnum.Active;

            Context.SaveChanges();
            return Success();
        }

        public UserInfo Create(string username, string password, string email)
        {
            if (Context.Users.Any(m => m.Username == username))
            {
                return null;
            }

            var user = new User
            {
                TimeCreated = DateTime.Now,
                Username = username,
                Email = email,
                Password = PasswordManager.EncryptPassword(password),
                Status = UserStatusEnum.Active
            };

            Context.Users.Add(user);
            Context.SaveChanges();

            return new UserInfo {
                Username = user.Username,
                TimeCreated = user.TimeCreated,
                Email = user.Email,
                Status = UserStatusEnum.Active
            };
        }

        public List<UserInfo> GetUserList()
        {
            var users = Context.Users.ToList();

            return users.Select(a => new UserInfo()
            {
                Username = a.Username,
                Email = a.Email,
                Status = a.Status,
                TimeCreated = a.TimeCreated
            })
                .ToList();
        }
    }
}