﻿using Foodo.Core.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Foodo.Core.Models
{
    public class AccountTransactionInfo
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public AccountTransactionSourceTypeEnum SourceType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public AccountTransactionStatusEnum Status { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public AccountTransactionTypeEnum Type { get; set; }

        public DateTime TimeCreated { get; set; }
        public decimal Amount { get; set; }
    }
}
