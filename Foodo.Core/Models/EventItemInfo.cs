﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace Foodo.Core.Models
{
    public class EventItemInfo
    {
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public string Username { get; set; }
    }
}
