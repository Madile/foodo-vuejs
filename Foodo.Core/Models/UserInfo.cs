﻿using Foodo.Core.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Foodo.Core.Models
{
    public class UserInfo : ServiceResponse
    {
        public UserInfo()
        {
            this.Events = new List<EventInfo>();
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public UserStatusEnum Status { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public DateTime TimeCreated { get; set; }
        public string Token { get; set; }
        public int Id { get; set; }
        public List<EventInfo> Events { get; set; }
    }
}
