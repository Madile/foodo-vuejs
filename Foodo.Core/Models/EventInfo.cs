﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace Foodo.Core.Models
{
    public class EventInfo
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public EventStatusEnum EventStatus { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public EventCategoryEnum EventCategory { get; set; }

        public string EventTitle { get; set; }
        public string CreatedUsername { get; set; }
        public string EventDescription { get; set; }
        public DateTime ValidUntil { get; set; }
        public DateTime TimeCreated { get; set; }
        public int Id { get; set; }
        public List<EventItemInfo> EventItems { get; set; }
    }
}
